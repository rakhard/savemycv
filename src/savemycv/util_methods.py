# SPDX-FileCopyrightText: 2023 Mikhail Rakhmanov <mikhail.rakhmanov@protonmail.com>
# SPDX-License-Identifier: MIT
import sys


def report(msg):
    sys.stderr.write(msg)
    sys.stderr.write("\n")
