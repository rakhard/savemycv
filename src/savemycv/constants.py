# SPDX-FileCopyrightText: 2023 Mikhail Rakhmanov <mikhail.rakhmanov@protonmail.com>
# SPDX-License-Identifier: MIT
import re

LANG_OPTIONS = {
    "eng": "English",
    "spa": "Spanish",
    "chi_sim": "Chinese",
    "rus": "Russian",
    "deu": "German",
    "fra": "French",
    "por": "Portuguese",
    "ita": "Italian",
    "nld": "Dutch",
    "swe": "Swedish",
    "dan": "Danish",
    "nor": "Norwegian",
    "pol": "Polish",
    "fin": "Finnish",
    "ces": "Czech",
    "hin": "Hindi",
}

VERBOSITY_OPTIONS = {
    "-v": "Default level of logging",
    "-vv": "Output ocrmypdf debug messages",
    "-vvv": "More detailed debugging from ocrmypdf and dependent modules",
}

CV_BUILDERS = ["Resume.io"]

RESUMEIO_URL_RE = re.compile(r"^https://ssr.resume.tools/to-image/([a-zA-Z0-9]{24})")
RESUMEIO_METADATA_URL_DIR = "https://ssr.resume.tools/meta/"
RESUMEIO_IMAGE_URL_DIR = "https://ssr.resume.tools/to-image/"

CV_BUILDERS_RE = {"resumeio": [RESUMEIO_URL_RE]}

LANG_URL_DIR = "https://github.com/tesseract-ocr/tessdata_best/raw/main/"

PROGRAM_NAME = "savemycv"

METADATA_WARNING = "The metadata field /CreationDate could not be copied to XMP"
