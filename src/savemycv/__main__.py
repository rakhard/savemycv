# SPDX-FileCopyrightText: 2023 Mikhail Rakhmanov <mikhail.rakhmanov@protonmail.com>
# SPDX-License-Identifier: MIT
import os
import sys
import requests
import subprocess
import errno

from ocrmypdf.api import configure_logging

from savemycv.cli import get_parser
from savemycv.constants import *
from savemycv.pdf_generator import PDFGenerator
from savemycv.util_methods import report


def _get_cv_id(url, pattern):
    return pattern.match(url).group(1)


def _validate_cv_id(metadata_url_dir, source_id):
    metadata_url = "".join([metadata_url_dir, source_id])

    try:
        response = requests.get(metadata_url)
        response.raise_for_status()
    except requests.exceptions.RequestException:
        import traceback

        traceback.print_exc()
        report("Failed to validate the CV ID.")
        sys.exit(1)


def _get_tesseract_path(command):
    try:
        output = subprocess.check_output(
            command, stderr=subprocess.STDOUT, shell=True, text=True
        )
    except subprocess.CalledProcessError:
        import traceback

        traceback.print_exc()
        report("tesseract is not in PATH, or the operating system is not supported.")
        sys.exit(1)
    return output


def _set_tessdata_path():
    tessdata_relative_path = "share/tessdata"

    if os.name == "nt":
        tesseract_path = _get_tesseract_path("where tesseract")
        env_library_path = os.path.dirname(os.path.dirname(tesseract_path))
        tessdata_relative_path = os.path.normpath(tessdata_relative_path)
    else:
        tesseract_path = _get_tesseract_path("which tesseract")
        env_library_path = os.path.dirname(tesseract_path)

    tessdata_path = os.path.join(env_library_path, tessdata_relative_path)

    if not os.path.exists(tessdata_path):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), tessdata_path)

    os.environ["TESSDATA_PREFIX"] = tessdata_path
    return tessdata_path


def _get_lang_data(data_path, langs):
    data_suffix = ".traineddata"

    for lang in langs:
        lang_filename = "".join([lang, data_suffix])
        lang_file_path = os.path.join(data_path, lang_filename)

        lang_url = "".join([LANG_URL_DIR, lang_filename])

        if not os.path.exists(lang_file_path):
            print("Downloading languages")
            try:
                response = requests.get(lang_url)
                response.raise_for_status()
            except requests.exceptions.RequestException:
                import traceback

                traceback.print_exc()
                report(f"Failed to download {lang_filename}")
                sys.exit(1)

            try:
                with open(lang_file_path, "wb") as f:
                    f.write(response.content)
            except OSError:
                import traceback

                traceback.print_exc()
                report(f"Failed to save {lang_filename}")
                sys.exit(1)


def main():
    args = get_parser()

    ocrmypdf_verbosity = args.verbosity - 1
    cv_url, cv_builder = args.url
    image_size = args.image_size

    ocr_langs = []

    if isinstance(args.lang, str):
        ocr_langs.append(args.lang)
    else:
        ocr_langs = args.lang

    configure_logging(verbosity=ocrmypdf_verbosity)

    tessdata_path = _set_tessdata_path()
    _get_lang_data(tessdata_path, ocr_langs)

    cv_id = _get_cv_id(cv_url, RESUMEIO_URL_RE)
    _validate_cv_id(RESUMEIO_METADATA_URL_DIR, cv_id)

    resumeio = PDFGenerator(
        cv_id=cv_id,
        ocr_langs=ocr_langs,
        image_size=image_size,
    )
    resumeio.run()


if __name__ == "__main__":
    main()
