# SPDX-FileCopyrightText: 2023 Mikhail Rakhmanov <mikhail.rakhmanov@protonmail.com>
# SPDX-License-Identifier: MIT
import argparse
from argparse import PARSER, REMAINDER, OPTIONAL, ZERO_OR_MORE, SUPPRESS, ONE_OR_MORE

from savemycv.constants import *
from savemycv.version import __version__


class ArgumentParser(argparse.ArgumentParser):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _get_values(self, action, arg_strings):
        if action.nargs not in [PARSER, REMAINDER]:
            try:
                arg_strings.remove("--")
            except ValueError:
                pass

        if not arg_strings and action.nargs == OPTIONAL:
            if action.option_strings:
                value = action.const
            else:
                value = action.default
            if isinstance(value, str):
                value = self._get_value(action, value)
                self._check_value(action, value)

        elif (
            not arg_strings
            and action.nargs == ZERO_OR_MORE
            and not action.option_strings
        ):
            if action.default is not None:
                value = action.default
            else:
                value = arg_strings
            self._check_value(action, value)

        elif len(arg_strings) == 1 and action.nargs in [None, OPTIONAL]:
            (arg_string,) = arg_strings
            if action.dest == "lang" and "," in arg_strings[0]:
                arg_strings_list = arg_strings[0].split(",")
                value = [self._get_value(action, v) for v in arg_strings_list]
                for v in value:
                    self._check_value(action, v)
            else:
                value = self._get_value(action, arg_string)
                self._check_value(action, value)

        elif action.nargs == REMAINDER:
            value = [self._get_value(action, v) for v in arg_strings]

        elif action.nargs == PARSER:
            value = [self._get_value(action, v) for v in arg_strings]
            self._check_value(action, value[0])

        elif action.nargs == SUPPRESS:
            value = SUPPRESS

        else:
            value = [self._get_value(action, v) for v in arg_strings]
            for v in value:
                self._check_value(action, v)
        return value


class ChoicesHelpFormatter(
    argparse.ArgumentDefaultsHelpFormatter, argparse.HelpFormatter
):
    def __init__(self, prog=PROGRAM_NAME, width=91, **kwargs):
        super().__init__(prog=prog, width=width, **kwargs)

    def _format_args(self, action, default_metavar):
        get_metavar = self._metavar_formatter(action, default_metavar)
        if action.nargs is None:
            if action.dest == "lang":
                result = "[%s,...]" % get_metavar(1)
            else:
                result = "%s" % get_metavar(1)
        elif action.nargs == OPTIONAL:
            result = "[%s]" % get_metavar(1)
        elif action.nargs == ZERO_OR_MORE:
            metavar = get_metavar(1)
            if len(metavar) == 2:
                result = "[%s [%s ...]]" % metavar
            else:
                result = "[%s ...]" % metavar
        elif action.nargs == ONE_OR_MORE:
            result = "%s [%s ...]" % get_metavar(2)
        elif action.nargs == REMAINDER:
            result = "..."
        elif action.nargs == PARSER:
            result = "%s ..." % get_metavar(1)
        elif action.nargs == SUPPRESS:
            result = ""
        else:
            try:
                formats = ["%s" for _ in range(action.nargs)]
            except TypeError:
                raise ValueError("invalid nargs value") from None
            result = " ".join(formats) % get_metavar(action.nargs)
        return result

    def _split_lines(self, text, width):
        lines = text.splitlines()
        wrapped_lines = []
        whitespace_matcher = self._whitespace_matcher
        import textwrap

        for line in lines:
            cleaned_line = whitespace_matcher.sub(" ", line).strip()
            wrapped = textwrap.wrap(cleaned_line, width)
            wrapped_lines.extend(wrapped)
        return wrapped_lines


def _validate_url_arg(url):
    for cv_builder, cv_builder_re in CV_BUILDERS_RE.items():
        if cv_builder_re[0].match(url):
            return url, cv_builder
        else:
            raise argparse.ArgumentTypeError(
                f"Unsupported CV builder or invalid URL format."
            )


def get_parser():
    lang_help = (
        "Language(s) of the CV or cover letter to be OCR'd. List command-line arguments separated by a comma for "
        "multiple languages (e.g., '-l eng,spa').\nResume.io supports the following languages:\n"
    )
    lang_help_choices = "\n".join(
        [f"{key}: {value}" for key, value in LANG_OPTIONS.items()]
    )

    verbosity_help = "Control the verbosity level of the program.\n"
    verbosity_help_choices = "\n".join(
        [f"{key}: {value}" for key, value in VERBOSITY_OPTIONS.items()]
    )

    formatted_cv_builders_list = ", ".join(CV_BUILDERS)

    parser = ArgumentParser(
        prog=PROGRAM_NAME,
        description=f"Generates a searchable PDF file from {formatted_cv_builders_list} CV images and the associated "
        f"CV metadata.",
        formatter_class=ChoicesHelpFormatter,
        add_help=False,
    )
    parser.add_argument(
        "-h",
        "--help",
        action="help",
        default=SUPPRESS,
        help="Show this help message and exit.",
    )
    parser.add_argument(
        "url",
        type=_validate_url_arg,
        help="The URL of the CV or cover letter to process.",
        metavar='"URL"',
    )
    parser.add_argument(
        "--lang",
        default="eng",
        choices=LANG_OPTIONS.keys(),
        help=lang_help + lang_help_choices,
        metavar="LANG",
        dest="lang",
    )
    parser.add_argument(
        "--image-size",
        default=5000,
        type=int,
        help="Set the source image width in pixels.",
        metavar="SIZE",
        dest="image_size",
    )
    parser.add_argument(
        "-v",
        action="count",
        default=0,
        help=verbosity_help + verbosity_help_choices,
        dest="verbosity",
    )
    parser.add_argument(
        "--version",
        action="version",
        version=__version__,
        help="Print the program's version number and exit.",
    )
    return parser.parse_args()
