# SPDX-FileCopyrightText: 2023 Mikhail Rakhmanov <mikhail.rakhmanov@protonmail.com>
# SPDX-License-Identifier: MIT
import os
import sys
import time
import requests
import json
import warnings
from dataclasses import dataclass, field
from io import BytesIO

from fpdf import FPDF
import pikepdf
import ocrmypdf

from savemycv.constants import *
from savemycv.util_methods import report


warnings.filterwarnings(
    "ignore",
    message=METADATA_WARNING,
    category=UserWarning,
    module="pikepdf.models.metadata",
)


@dataclass
class PDFGenerator:
    cv_id: str
    ocr_langs: list
    image_size: int
    images_urls: list = field(default_factory=lambda: [])
    cache_timestamp: str = time.time()
    image_url: str = "".join(
        [
            RESUMEIO_IMAGE_URL_DIR,
            "{cv_id}-{page_id}.png?cache={cache_timestamp}&size={img_size}",
        ]
    )

    metadata_url: str = "".join(
        [
            RESUMEIO_METADATA_URL_DIR,
            "{cv_id}?cache={cache_timestamp}",
        ]
    )

    def _get_cv_metadata(self):
        request = requests.get(
            self.metadata_url.format(
                cv_id=self.cv_id, cache_timestamp=self.cache_timestamp
            )
        )
        self.metadata = json.loads(request.text).get("pages")

    def _format_images_urls(self):
        for page_id in range(1, len(self.metadata) + 1):
            download_url = self.image_url.format(
                cv_id=self.cv_id,
                page_id=page_id,
                cache_timestamp=self.cache_timestamp,
                img_size=self.image_size,
            )
            self.images_urls.append(download_url)

    def _generate_pdf(self):
        print("Generating PDF file")
        width, height = self.metadata[0].get("viewport").values()

        # noinspection PyTypeChecker
        pdf = FPDF(unit="pt", format=(width, height))
        pdf.set_margin(0)
        pdf.set_auto_page_break(False)

        for i, image_url in enumerate(self.images_urls):
            pdf.add_page()
            pdf.image(image_url, w=width, h=height)

            for link in self.metadata[i].get("links"):
                horizontal_pos = link["left"]
                vertical_pos = height - link["top"] - link["height"]

                pdf.link(
                    x=horizontal_pos,
                    y=vertical_pos,
                    w=link["width"],
                    h=link["height"],
                    link=link["url"],
                )
        self.buffer = pdf.output()

    def _perform_ocr(self):
        ocrmypdf.ocr(
            BytesIO(self.buffer),
            "output.pdf",
            language=self.ocr_langs,
            output_type="pdf",
            optimize=0,
            fast_web_view=0,
        )

    @staticmethod
    def _remove_pdf_metadata():
        cv_name = "output.pdf"
        cv_path = os.path.join(os.getcwd(), cv_name)

        try:
            with pikepdf.open(cv_name, allow_overwriting_input=True) as pdf:
                del pdf.Root.Metadata
                # noinspection PyPropertyAccess
                del pdf.docinfo
                pdf.save(cv_name)

        except OSError:
            import traceback

            traceback.print_exc()
            report("Failed to remove the metadata.")
            sys.exit(1)

        print(f"The CV has been saved to {cv_path}")

    def run(self):
        self._get_cv_metadata()
        self._format_images_urls()
        self._generate_pdf()
        self._perform_ocr()
        self._remove_pdf_metadata()
