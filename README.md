<!-- SPDX-FileCopyrightText: 2023 Mikhail Rakhmanov <mikhail.rakhmanov@protonmail.com> -->
<!-- SPDX-License-Identifier: MIT -->
[![Project Status: Concept – Minimal or no implementation has been done yet, or the repository is only intended to be a limited example, demo, or proof-of-concept.](https://www.repostatus.org/badges/latest/concept.svg)](https://www.repostatus.org/#concept)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![License](https://img.shields.io/badge/license-MIT-green)](LICENSE)

# savemycv

Generates a searchable PDF file from [Resume.io](https://resume.io/) CV images and the associated CV metadata.

## Prerequisites

Download and install [Miniconda](https://docs.conda.io/projects/miniconda/en/latest/index.html).

## Installation

1. Open your terminal and navigate to the "savemycv" directory:

   ```bash
   cd savemycv
   ```

2. Create a Conda environment using the provided environment.yml file:

   ```bash
   conda env create -f environment.yml
   ```

3. Activate the "savemycv" Conda environment:

   ```bash
   conda activate savemycv
   ```    

4. Build the project:

   ```bash
   python -m build
   ```

5. Install savemycv package:

   ```bash
   pip install dist/savemycv-0.1.0.tar.gz
   ```

## Usage

1. Go to https://resume.io/app and click on either the "Resumes" or "Cover Letters" tab, depending on what you want to
   download.
2. Press F12 to open DevTools.
3. Click on the "Sources" tab.
4. In the "Page" tab, locate and expand the "ssr.resume.tools" directory.
5. Inside the "to-image" catalog, you will find an image of your resume.
6. Right-click on the image and select "Copy address link".
7. Run savemycv with the copied URL as an argument:

   ```bash
   savemycv "your_cv_url"
   ```

   > **Note:** The URL is enclosed in quotes to prevent special characters contained in the URL from being
   > misinterpreted by the command line. Alternatively, you can avoid enclosing the URL in quotes by removing all
   > characters after the ".webp" extension. In any case, the cache identifier and image size will be automatically set
   > at runtime.

   By default, the text layer is generated in English. You can specify a second language used in your resume.

   ```bash
   savemycv --lang eng,spa "your_cv_url"
   ```

   You can also specify a custom size for the downloaded image for resume generation. The default image size is the
   largest available, which is 5000 pixels in width.

   ```bash
   savemycv --image-size 2500 "your_cv_url"
   ```
